# Admin client certificates

# Only add, don't remove!
locals {
  clients = "${compact(split("\n", file("names.txt")))}"
}

resource "tls_private_key" "private_key" {
  count     = "${length(local.clients)}"
  algorithm = "RSA"
}

resource "tls_cert_request" "csr" {
  count = "${length(local.clients)}"

  key_algorithm   = "${tls_private_key.private_key.*.algorithm[count.index]}"
  private_key_pem = "${tls_private_key.private_key.*.private_key_pem[count.index]}"

  subject {
    common_name         = "${element(local.clients, count.index)}"
    organization        = "Stichting Internet Cleanup Foundation"
    organizational_unit = "Faalkaart"
  }
}

resource "tls_locally_signed_cert" "signed_certificate" {
  count            = "${length(local.clients)}"
  cert_request_pem = "${tls_cert_request.csr.*.cert_request_pem[count.index]}"

  ca_key_algorithm   = "${tls_private_key.root.algorithm}"
  ca_private_key_pem = "${tls_private_key.root.private_key_pem}"
  ca_cert_pem        = "${tls_self_signed_cert.root.cert_pem}"

  # 5 years valid
  validity_period_hours = 43800
  early_renewal_hours   = 8760

  allowed_uses = ["client_auth"]
}

# Write key and cert to file
resource "local_file" "key" {
  count    = "${length(local.clients)}"
  content  = "${tls_private_key.private_key.*.private_key_pem[count.index]}"
  filename = "keys/${element(local.clients, count.index)}.pem"
}

resource "local_file" "certificate" {
  count    = "${length(local.clients)}"
  content  = "${tls_locally_signed_cert.signed_certificate.*.cert_pem[count.index]}"
  filename = "certs/${element(local.clients, count.index)}.pem"
}
