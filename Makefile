password := geheim

keys = $(basename $(notdir $(wildcard keys/*.pem)))
$(info ${keys})
pkcs12: $(addprefix keys/,$(addsuffix .p12, ${keys}))

keys/%.p12: keys/%.pem certs/%.pem
	# verifying relationship
	openssl verify -CAfile certs/ca.pem certs/$*.pem
	# export to p12
	/usr/bin/openssl pkcs12 -export -passout pass:${password} \
		-inkey keys/$*.pem \
		-in certs/$*.pem \
		-out keys/$*.p12 -name '$*'

clean:
	rm -f keys/*.p12
