This is the Failmap private CA. It uses Hashicorp Terraform to automate the PKI process. All confidential information in this repository is encrypted by GPG using git-crypt.

Requirements:

- [Terraform](https://www.terraform.io/downloads.html)
- [Git-crypt](https://github.com/AGWA/git-crypt/blob/master/INSTALL.md)
- make

# Creating a new client certificate

Ensure the repository is unlocked using `git-crypt`, see below.

Add a new client name to `clients` in `certificates.tf`.

Run: `terraform apply`

Key and Certificate PEM files with the client name are created in the `keys` and `certs` folders.

To export keys as PKCS12 (macOS Keychain) run: `make pkcs12`. A PKCS12 file with the client name is created in the `keys` folder.

Secrets are kept in the `.tfstate` file. PKCS12 keys are not recorded in Git as they can be regenerated.

Add the Terraform state and Certificates to Git, commit and push to upstream.

# Git-crypt
This repository contains files that are GPG encrypted (see: `.gitattributes`). The `git-crypt` tool managed this encryption transparantly. In order to access the encrypted files a `unlock` needs to be performed (only once, unless repository is locked again). Your GPG key needs to be added to the repository `.git-crypt/keys/default/0` in order for you to unlock it. See below for more instructions.

## Unlocking
If your GPG key is in the list of allowed keys the repository can be unlocked using:

    git-crypt unlock

Git-crypt will transparently (de/en)crypt files as they are checked-out/commited from/to the repository.

To verify if the repository is unlocked check if encrypted files are readable (non-binary), eg: `terraform.tfstate` should be a JSON file.

## Adding a user to git-crypt
This step is only required if someone needs to be able to create client certificates, not if a client certificate is to be issued to this person.

To allow a new user to unlock the repository first make sure the repository is unlocked. Then make sure the users GPG key is in your keychain. Run this command to add the user:

    git-crypt add-gpg-user <GPG-USER-ID/EMAIL>

A new commit will be created which records this user's permission to unlock the repository. Push this commit upstream.
