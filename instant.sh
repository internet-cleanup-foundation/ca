#!/bin/sh
# usage: ./instant.sh <name>

# exit on failure, be verbose
set -ex

name=$1

# sanity
if ! test -z "$(git diff --shortstat 2> /dev/null | tail -n1)"; then
  echo "Unsaved changes, not continuing!"
  exit 1
fi

# setup
gpg-agent --version
git-crypt unlock

# add new name
echo "$name" >> names.txt

# create key and certificate
terraform apply

# create .p12 file
make pkcs12

# record in git history
git add -A
git commit -m "added a new user"

# upload to git
git push

git-crypt lock

# UX
open keys/
echo "send the key over GPG"
